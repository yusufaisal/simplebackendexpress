const line = require('@line/bot-sdk');
const TelegramBot = require('node-telegram-bot-api');
const Contact = require('./models/model.contact.js');
const Chat = require('./models/model.chat.js');

exports.send_to_line = (messager_access, req, res) => {
    const text = req.body.message;
    const userID =req.body.userid;

    const config_line = {
        channelAccessToken: messager_access.channel_access_token,
        channelSecret: messager_access.channel_secret
    };
    const client = new line.Client(config_line);

    const message = { type: 'text', text: text };
    client.pushMessage(userID,message)
    .then(data  => {
        res.sendStatus(200)
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Something wrong while push the message."
        });
    });
}

exports.send_to_telegram = (messager_access, req, res) => {
    const text = req.body.message;
    const userID = req.body.userid;

    const token = messager_access.channel_access_token;
    const client = new TelegramBot(token, {polling: false});

    client.sendMessage(userID,text)
    .then(data  => {
        res.sendStatus(200)
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Something wrong while push the message."
        });
    });
}

// EVENT HANDLER 
exports.telegram_handle_event = async (message, messenger_id, res) => {
    if (message.text == undefined) {
        return Promise.resolve(null);
      }
  
      const query = {messenger_id:messenger_id,user_id:message.from.id};
      const response = await Contact.find(query);
      if (!response[0]){
          console.log("creating new contact")
          const new_contact = new Contact({
              user_id: message.from.id,
              name: " ".concat([message.from.first_name, message.from.last_name]),
              profile_pic: "",
              messenger_id: messenger_id,
          })
          new_contact.save()
          .then(contact => {
              save_chat(contact, message.text, res)
          }).catch(err => {
              res.status(500).send({
                  message: err.message || "Something wrong while creating the contact."
              });
          });
      } else {
          const contact = response[0]
          save_chat(contact, message.text, res)
      }
      return true
}

exports.line_handle_event = async (event, messenger_id, res) => {
    if (event.type !== 'message' || event.message.type !== 'text') {
      return Promise.resolve(null);
    }

    const query = {messenger_id:messenger_id,user_id:event.source.userId};
    const response = await Contact.find(query);
    if (!response[0]){
        console.log("creating new contact")
        const new_contact = new Contact({
            user_id: event.source.userId,
            name: "", 
            profile_pic: "",
            messenger_id: messenger_id,
        })
        new_contact.save()
        .then(contact => {
            save_chat(contact,event.message.text, res)
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Something wrong while creating the contact."
            });
        });
    } else {
        const contact = response[0]
        save_chat(contact, event.message.text ,res)
    }
    return true
}

function save_chat(contact,text, res){
    const new_chat = new Chat({
        text: text,
        media: null,
        video: null,
        send_time: null,
        recive_time: Date.now(),
        contact_id: contact._id
    })

    new_chat.save()
    .then(chat =>{
        res.sendStatus(200)
    }).catch(err => {
        res.sendStatus(500)
    })
}