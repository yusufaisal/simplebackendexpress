const mongoose = require('mongoose');

const MessangerAccesSchema = mongoose.Schema({
    channel_access_token : String,
    channel_secret : String,
    business_id: String,
    messenger_name: String,
},{
    timestamps: true,
})

module.exports = mongoose.model('MessangerAccess', MessangerAccesSchema);