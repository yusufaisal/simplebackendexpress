const mongoose = require('mongoose');

const ContactSchema =  mongoose.Schema({
    user_id: String,
    name: String, 
    profile_pic: String,
    messenger_id: String,
},{
    timestamps: true
});

module.exports = mongoose.model('Contacts', ContactSchema);