const mongoose = require('mongoose');

const BusinessSchema = mongoose.Schema({
    auth_token : String,
    business_name: String,
    category: String,
    disabled: Boolean
},{
    timestamps: true
})

module.exports = mongoose.model('Business', BusinessSchema);