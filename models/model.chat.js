const mongoose = require('mongoose');

const ChatSchmema = mongoose.Schema({
    text: String,
    media: String,
    video: String,
    send_time: Date,
    recive_time: Date,
    contact_id: String,
    is_read:Boolean
},{
    timestamps: true
});

module.exports = mongoose.model('Chats', ChatSchmema);