const mongoose = require('mongoose');

module.exports = SettingSchema = mongoose.Schema({
    business_id: String,
    is_wa_available: Boolean,
    is_tl_available: Boolean,
    is_ln_available: Boolean,
},{
    timestamps: true
})