// get dependencies
const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config.js');
const mongoose = require('mongoose');

const MessangerAccess = require('./models/model.MessangerAccess.js');

const Handler = require('./messenger.handler.js');
const app = express();
const messengers = {
    LINE: "Line",
    TELEGRAM: "Telegram",
    WHATSAPP: "WhatsApp",
} 

// Parse requests
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Enable CORS for all HTTP methods
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
  
// Connecting to the database
mongoose.Promise = global.Promise;
mongoose.connect(config.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// Define route
app.get('/', (req, res) => {
    res.json({"message": "I Love You 3000"});
});
app.post('/send-message/', (req, res) =>{
    if(!req.body) {
        return res.status(400).send({
            message: "message can not be empty"
        });
    }

    const query ={
        'business_id': req.body.business_id,
        'messenger_name': req.body.messenger_name
    }

    MessangerAccess.find(query)
    .then(messager_access => {
        if (messager_access[0]!=undefined) {
            switch (messager_access[0].messenger_name){
                case "Line":
                    console.log("LINE");
                    Handler.send_to_line(messager_access[0], req, res);
                    break
                case "Telegram":
                    console.log("Telegram");
                    Handler.send_to_telegram(messager_access[0], req, res);
                    break
                case "WhatsApp":
                    console.log("WhatsApp");
                    break
            }
        } else {
            res.status(200).send({
                message: "not found"
            })
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Something wrong while finding the messanger access."
        });
    })
});

app.post('/webhook-tg/:messenger_id', (req, res) => {
    Promise
    .all([Handler.telegram_handle_event(req.body.message, req.params.messenger_id, res)])
    .then((result) => {
        res.status(200).json(result);
    })
    .catch((err) => {
        res.status(500).end();
  });
})

app.post('/webhook-ln/:messenger_id', (req, res) => {
    Promise
    .all(req.body.events.map((event) => Handler.line_handle_event(event, req.params.messenger_id, res)))
    .then((result) => {
        res.status(200).json(result);
    })
    .catch((err) => {
        res.status(500).end();
    });
});

app.listen(process.env.PORT || config.serverport, () => {
    console.log("Server is listening on port", process.env.PORT || config.serverport);
});
